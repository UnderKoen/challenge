package nl.underkoen.challenge.hard.rubikscube.entities;

/**
 * Created by Under_Koen on 12/11/2018.
 */
public enum Moves {
    /**
     * De meest linkse rij (verticaal) of een bovenste rij (horizontaal).
     */
    LEFT(0),

    /**
     * De middelste rij (verticaal of horizontaal)
     */
    MIDDLE(1),

    /**
     * De meest rechtse rij (verticaal) of een onderste rij (horizontaal).
     */
    RIGHT(2);

    private int id;

    Moves(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
