package nl.underkoen.challenge.hard.rubikscube.entities;

/**
 * Created by Under_Koen on 12/11/2018.
 */
public enum Color {
    GREEN(0),
    RED(1),
    BLUE(2),
    ORANGE(3),
    YELLOW(4),
    WHITE(5);

    private int id;

    Color(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Color getColor(int id) {
        switch (id) {
            case 0:
                return GREEN;
            case 1:
                return RED;
            case 2:
                return BLUE;
            case 3:
                return ORANGE;
            case 4:
                return YELLOW;
            case 5:
                return WHITE;
            default:
                throw new IllegalArgumentException();
        }
    }
}
