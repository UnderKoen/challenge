package nl.underkoen.challenge.hard.rubikscube.entities;

/**
 * Created by Under_Koen on 12/11/2018.
 */
public enum Face {
    FRONT(0),
    RIGHT(1),
    BACK(2),
    LEFT(3),
    BOTTOM(4),
    TOP(5);

    private int id;

    Face(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
