package nl.underkoen.challenge.hard.rubikscube.entities;

/**
 * Deze class moet een constructor hebben zonder argumenten.
 */
public interface Cube {
    /**
     * @param face De kant van waar je de kleuren wilt weten.
     * @return Een array met een lengte van 9 die van boven naar beneden en van links naar rechts georderd zijn.
     */
    Color[] getFace(Face face);

    /**
     * @param move De rij die je wil roteren.
     * @param direction Welke kant je wilt roteren.
     */
    void move(Moves move, Direction direction);

    /**
     * Roteerde cube zodat er een andere kant als front is.
     * @param direction Welke kant om te draaien.
     */
    void rotate(Direction direction);

    /**
     * @return een cube objrct die dezelfde values heeft als de object waar de functie gerunned wordt.
     */
    Cube clone();
}
