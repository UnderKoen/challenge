package nl.underkoen.challenge.hard.rubikscube;

import nl.underkoen.challenge.TestingError;
import nl.underkoen.challenge.hard.rubikscube.entities.*;

import java.util.Arrays;
import java.util.Random;

import static nl.underkoen.challenge.hard.rubikscube.entities.Face.*;
import static nl.underkoen.challenge.hard.rubikscube.entities.Color.*;
import static nl.underkoen.challenge.hard.rubikscube.entities.Face.LEFT;
import static nl.underkoen.challenge.hard.rubikscube.entities.Face.RIGHT;

/**
 * Created by Under_Koen on 12/11/2018.
 */
public class RubiksTest {
    private static Random random = new Random(133725072002L);

    private Class<? extends Cube> cls;

    public RubiksTest(Class<? extends Cube> cls) {
        this.cls = cls;
    }

    public void test() throws TestingError {
        //Test init values
        Cube cube = newInstance();
        testFace(cube, FRONT, 0);
        testFace(cube, RIGHT, 1);
        testFace(cube, BACK, 2);
        testFace(cube, LEFT, 3);
        testFace(cube, BOTTOM, 4);
        testFace(cube, TOP, 5);

        for (int i = 0; i < 20; i++) {
            Cube cb = newInstance();
            for (int j = 0; j < 15; j++) {
                Direction dir = Direction.values()[random.nextInt(Direction.values().length)];
                testRotation(cb, dir);
            }
        }
    }

    private Cube newInstance() throws TestingError {
        try {
            return cls.getConstructor().newInstance();
        } catch (Exception e) {
            throw new TestingError();
        }
    }

    private void testMove(Cube cube, Moves move, Direction direction) throws TestingError {
        Cube oldCube = cube.clone();
        cube.move(move, direction);
    }

    private void testRotation(Cube cube, Direction direction) throws TestingError {
        Cube oldCube = cube.clone();
        cube.rotate(direction);
        switch (direction) {
            case UP:
                testFace(oldCube.getFace(FRONT), cube.getFace(BOTTOM));
                testFace(oldCube.getFace(TOP), cube.getFace(FRONT));
                testFlip(oldCube.getFace(BACK), cube.getFace(TOP));
                testFlip(oldCube.getFace(BOTTOM), cube.getFace(BACK));
                testRotation(oldCube.getFace(LEFT), cube.getFace(LEFT), true);
                testRotation(oldCube.getFace(RIGHT), cube.getFace(RIGHT), false);
                break;
            case DOWN:
                testFace(cube.getFace(FRONT), oldCube.getFace(BOTTOM));
                testFace(cube.getFace(TOP), oldCube.getFace(FRONT));
                testFlip(cube.getFace(BACK), oldCube.getFace(TOP));
                testFlip(cube.getFace(BOTTOM), oldCube.getFace(BACK));
                testRotation(cube.getFace(LEFT), oldCube.getFace(LEFT), true);
                testRotation(cube.getFace(RIGHT), oldCube.getFace(RIGHT), false);
                break;
            case LEFT:
                testFace(cube.getFace(FRONT), oldCube.getFace(LEFT));
                testFace(cube.getFace(LEFT), oldCube.getFace(BACK));
                testFace(cube.getFace(BACK), oldCube.getFace(RIGHT));
                testFace(cube.getFace(RIGHT), oldCube.getFace(FRONT));
                testRotation(cube.getFace(TOP), oldCube.getFace(TOP), true);
                testRotation(cube.getFace(BOTTOM), oldCube.getFace(BOTTOM), false);
                break;
            case RIGHT:
                testFace(oldCube.getFace(FRONT), cube.getFace(LEFT));
                testFace(oldCube.getFace(LEFT), cube.getFace(BACK));
                testFace(oldCube.getFace(BACK), cube.getFace(RIGHT));
                testFace(oldCube.getFace(RIGHT), cube.getFace(FRONT));
                testRotation(oldCube.getFace(TOP), cube.getFace(TOP), true);
                testRotation(oldCube.getFace(BOTTOM), cube.getFace(BOTTOM), false);
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

    private void testFlip(Color[] face, Color[] oldFace) throws TestingError {
        Color[] rot = new Color[9];
        for (int i = 0; i < face.length; i++) {
            rot[i] = getColor(8 - face[i].getId());
        }
        testFace(rot, oldFace);
    }

    private void testRotation(Color[] face, Color[] oldFace, boolean side) throws TestingError {
        Color[] rot = new Color[9];
        for (int i = 0; i < face.length; i++) {
            int j;
            switch (i) {
                case 0:
                    j = 2;
                    break;
                case 1:
                    j = 5;
                    break;
                case 2:
                    j = 8;
                    break;
                case 3:
                    j = 1;
                    break;
                case 4:
                    j = 4;
                    break;
                case 5:
                    j = 7;
                    break;
                case 6:
                    j = 0;
                    break;
                case 7:
                    j = 3;
                    break;
                case 8:
                    j = 6;
                    break;
                default:
                    j = -1;
                    break;
            }
            rot[j] = (side) ? face[i] : getColor(8 - face[i].getId());
        }
        testFace(rot, oldFace);
    }

    private void testFace(Color[] face1, Color[] face2) throws TestingError {
        if (face1.length < 9 || face2.length < 9) throw new TestingError();
        for (int i = 0; i < face1.length; i++) {
            if (face1[i] != face2[i]) throw new TestingError();
        }
    }

    private void testFace(Cube cube, Face face, int... colors) throws TestingError {
        if (colors.length == 0) throw new TestingError();
        if (colors.length < 9) {
            int val = colors[0];
            colors = new int[9];
            Arrays.fill(colors, val);
        }

        Color[] faceColors = cube.getFace(face);
        for (int i = 0; i < colors.length; i++) {
            if (faceColors[i].getId() != colors[i]) throw new TestingError();
        }
    }
}
