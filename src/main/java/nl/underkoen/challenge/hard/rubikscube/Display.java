package nl.underkoen.challenge.hard.rubikscube;

import nl.underkoen.challenge.hard.rubikscube.entities.Color;
import nl.underkoen.challenge.hard.rubikscube.entities.Cube;
import nl.underkoen.challenge.hard.rubikscube.entities.Face;

/**
 * Created by Under_Koen on 13/11/2018.
 */
public class Display {
    private static final String EMPTY_LINE = "              ";
    private static final String ROW = "|---|---|---|";
    private static final Face[] ORDER = {Face.LEFT, Face.FRONT, Face.RIGHT, Face.BACK};

    public static void print(Cube cube) {
        for (String l : textFace(cube.getFace(Face.TOP), true)) {
            System.out.println(l);
        }

        System.out.println();

        String[][] lines = new String[7][4];
        for (int i = 0; i < ORDER.length; i++) {
            lines[i] = textFace(cube.getFace(ORDER[i]), false);
        }

        for (int i = 0; i < 7; i++) {
            StringBuilder line = new StringBuilder();
            for (int j = 0; j < 4; j++) {
                line.append(lines[j][i]);
                line.append(" ");
            }
            System.out.println(line.toString());
        }

        System.out.println();

        for (String l : textFace(cube.getFace(Face.BOTTOM), true)) {
            System.out.println(l);
        }
    }

    private static String[] textFace(Color[] colors, boolean emptyLine) {
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            if (emptyLine) line.append(EMPTY_LINE);
            if (i % 2 == 0) {
                line.append(ROW);
            } else {
                int start = (i - 1) / 2 * 3;
                line.append("|");
                for (int j = start; j < start + 3; j++) {
                    line.append(" ");
                    line.append(colors[j].name().charAt(0));
                    line.append(" |");
                }
            }
            line.append("\n");
        }
        return line.toString().split("\n");
    }
}
