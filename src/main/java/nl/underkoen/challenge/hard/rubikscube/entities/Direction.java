package nl.underkoen.challenge.hard.rubikscube.entities;

/**
 * Created by Under_Koen on 12/11/2018.
 */
public enum Direction {
    UP(0),
    DOWN(1),
    LEFT(2),
    RIGHT(3);

    private int id;

    Direction(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
