package nl.underkoen.challenge.basic.console;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A4 {
    public static void main(String[] args) {
        //If: je kan dingen laten gebeuren als een statement waar is.
        //Dus bijvoorbeeld:
        if (10 > 6) {
            //Deze code wordt uitgevoerd als 10 groter is dan 6 wat in dit geval waar is.
        } else {
            //Deze code wordt uitgevoerd als 10 niet groter is dan 6 wat in dit geval niet waar is.
        }

        //Je kan een String ook vergelijken met bijvoorbeeld "test".equals("test").
        //Je kan ook een variable gebruiken in plaats van "test"
        //Probeer is te kijken of je "wachtwoord" ingevoerd hebt en als dit waar of niet waar is een bericht te sturen.

    }
}
