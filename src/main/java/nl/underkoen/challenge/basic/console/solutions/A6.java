package nl.underkoen.challenge.basic.console.solutions;

import java.util.Scanner;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A6 {
    public static void main(String[] args) {
        //Sommen: je kan ook strings samenvoegen of getallen op tellen
        //Probeer nu je code van A5 zo aan te passen dat je weet welke input van je fout is.
        //Maak nu een while loop dat je het wachtwoord moet blijven raden totdat je het goed hebt zoals in A4.
        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine();

        while (!text.equals("wachtwoord")) {
            System.out.println(text + " is WRONG");
            text = scanner.nextLine();
        }

        System.out.println("CORRECT");

    }
}
