package nl.underkoen.challenge.basic.console;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A5 {
    public static void main(String[] args) {
        //While: hiermee blijf je iets uit voeren zolang de statement waar is.
        //Bijvoorbeeld:
        String s = "test";

        //Je kan een ! ergens voor zetten en dan wordt het omgekeerd.
        while (!s.equals("test")) {
            //Deze code blijft uitgevoerd worden zolang s niet "test" is.
            //Door NAAM = WAARDE hier te gebruiken varander je de waarde van de variable
            s = "haha";
            //Dit heeft als gevolg dat deze while-loop ophoudt
        }

        //Maak nu een while loop dat je het wachtwoord moet blijven raden totdat je het goed hebt zoals in A4.

    }
}
