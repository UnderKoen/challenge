package nl.underkoen.challenge.basic.console.solutions;

import java.util.Scanner;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A5 {
    public static void main(String[] args) {
        //Maak nu een while loop dat je het wachtwoord moet blijven raden totdat je het goed hebt zoals in A4.
        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine();

        while (!text.equals("wachtwoord")) {
            System.out.println("WRONG");
            text = scanner.nextLine();
        }

        System.out.println("CORRECT");
    }
}
