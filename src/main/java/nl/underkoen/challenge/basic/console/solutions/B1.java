package nl.underkoen.challenge.basic.console.solutions;

import java.util.Scanner;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class B1 {
    public static void main(String[] args) {
        //Rekenmachine: probeer nu is een basic rekenmachine te maken waar je twee getalen kan op tellen.
        //Je hebt hier voor scanner.nextInt() nodig.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wat is het eerste getal?");
        int first = scanner.nextInt();

        System.out.println("Wat is het tweede getal?");
        int second = scanner.nextInt();

        int antwoord = first + second;

        System.out.println(first + " + " + second + " = " + antwoord);

    }
}