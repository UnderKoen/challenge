package nl.underkoen.challenge.basic.console.solutions;

import java.util.Scanner;

/**
 * Created by Under_Koen on 15/11/2018.
 */
public class B2 {
    public static void main(String[] args) {
        //Je gebruikt een switch als je veel waardes hebt waar voor je wilt testen.
        //Voorbeeld
        switch ("test") {
            case "hey":
                //Do stuff
                break;
            case "test":
                //Do stuff
                break;
            default:
                //When not one of the above
                break;
        }

        //Voeg -, * en / door toe aan je reken machine.
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wat is het eerste getal?");
        int first = scanner.nextInt();

        String thing = scanner.next();

        System.out.println("Wat is het tweede getal?");
        int second = scanner.nextInt();

        int result = 0;
        switch (thing) {
            case "+":
                result = first + second;
                break;
            case "-":
                result = first - second;
                break;
            case "/":
                result = first / second;
                break;
            case "*":
                result = first * second;
                break;
            default:
                System.out.println("UGH.....");
                break;
        }

        System.out.println(first + " " + thing + " " + second + " = " + result);
    }
}
