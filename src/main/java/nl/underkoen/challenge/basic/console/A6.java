package nl.underkoen.challenge.basic.console;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A6 {
    public static void main(String[] args) {
        //Sommen: je kan ook strings samenvoegen of getallen op tellen
        //Bijvoorbeeld
        String s = "He" + "y"; //s is dus "Hey"
        int i = 10 + 5; //en i is dan 15

        //Probeer nu je code van A5 zo aan te passen dat je weet welke input van je fout is.
    }
}
