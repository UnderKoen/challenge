package nl.underkoen.challenge.basic.console;

import java.util.Scanner;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A2 {
    public static void main(String[] args) {
        //Hier maak ik een scanner aan dit een object die je input uit console kan halen.
        Scanner scanner = new Scanner(System.in);

        //Met scanner.nextLine() krijg je de text dat je in console typed, de methode wacht totdat je op enter hebt getyped.
        //Probeer de text die je hebt in getyped in de console te schrijven.

    }
}
