package nl.underkoen.challenge.basic.console.solutions;

import java.util.Scanner;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String text = scanner.nextLine();

        if(text.equals("wachtwoord")){
            System.out.println("CORRECT");
        } else {
            System.out.println("WRONG");
        }
    }
}

