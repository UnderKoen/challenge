package nl.underkoen.challenge.basic.methods;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A2 {
    //Dit is een methode met parameters.
    //De parameters geef je aan door tussen de haakje het TYPE en NAAM tezetten zoals hier onder.
    //Je kan ook zoals je hier staat verschillende parameters gebruiken.
    public static void voorbeeld(String s, int i) {
        //Deze code wordt gerunned.
    }

    public static void main(String[] args) {
        //Als je een methode met parameters wilt uitvoeren doe je dat zo
        voorbeeld("hey", 10);

        //Run je methode met de volgende namen ["Jan", "Peter", "World"]
    }

    //Maak een methode waarmee je "hello" zegt tegen de parameter hey(String)
}
