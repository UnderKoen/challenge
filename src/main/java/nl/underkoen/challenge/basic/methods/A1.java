package nl.underkoen.challenge.basic.methods;

/**
 * Created by Under_Koen on 14/11/2018.
 */
public class A1 {
    //Dit is een methode.
    //Je kan een methode maken door \/ te gebuiken/
    //public/private static void NAAM()
    //public geeft aan dat iedereen vanaf elke class er bij kan.
    //private geeft aan dat je er alleen bij kan van af de class waar het in staat.
    public static void voorbeeld() {
        //Deze code wordt gerunned.
    }

    public static void main(String[] args) {
        //Je kan een methode runnen door achter de naam haakjes te zetten
        voorbeeld();

        //Run je hello world methode
    }

    //Maak nu een private methode die "Hello world" in de console zet.
}