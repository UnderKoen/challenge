package nl.underkoen.challenge;

import nl.underkoen.challenge.hard.rubikscube.RubiksTest;
import nl.underkoen.challenge.hard.rubikscube.entities.Cube;

/**
 * Created by Under_Koen on 12/11/2018.
 */
public class Main {
    public static final Class<? extends Cube> CUBE_CLASS = null; //TODO voeg hier de class toe van je cube.

    public static void main(String[] args) throws TestingError {
        if (CUBE_CLASS != null) new RubiksTest(CUBE_CLASS).test();
    }
}